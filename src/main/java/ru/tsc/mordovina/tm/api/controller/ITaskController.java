package ru.tsc.mordovina.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
