package ru.tsc.mordovina.tm;

import ru.tsc.mordovina.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final ru.tsc.mordovina.tm.component.Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
